class RepresentorException(ValueError):
    pass


class ParseException(RepresentorException):
    pass


class FormatException(RepresentorException):
    pass

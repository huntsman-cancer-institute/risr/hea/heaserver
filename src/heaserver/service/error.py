from heaobject.error import HEAException

class StartException(HEAException):
    pass

class DatabaseStartException(StartException):
    pass

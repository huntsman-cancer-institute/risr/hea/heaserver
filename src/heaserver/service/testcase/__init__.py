"""
Modules for testing HEA.

Marked modules, classes, and functions depend on the moto and/or testcontainers packages.
"""
from heaobject.user import TEST_USER

-e .
idna~=3.3
pytest~=6.2.5
twine~=3.7.1
wheel~=0.37.1
git+https://github.com/arpost/testcontainers-python@1106e3bd#testcontainers
mypy==0.942
pip-licenses~=3.5.3
aiohttp-swagger3~=0.7.2
moto~=3.1.4
pytest-xdist[psutil]~=2.5.0
pytest-cov~=4.0.0

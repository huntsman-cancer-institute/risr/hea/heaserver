from typing import Dict
import boto3
import json
import os


def lambda_handler(event, context):
    """
    Invoked when the aws api gateway is called it is responsible for assuming a role in the organization's
    member account.

    :param event: contains the decoded id token's claims and other information about the request received through the api gateway.
     The event also has query params requried to assume the role
    :return json response containing the account, password, session token and the session token's expiration.
    """
    sts_connection = boto3.client('sts')
    sub = None
    print(os.environ['AWS_SECRET_ACCESS_KEY'])

    if(event['requestContext'] and event['requestContext']['authorizer'] and event['requestContext']['authorizer']['jwt']
        and event['requestContext']['authorizer']['jwt']['claims'] ):
        sub = event['requestContext']['authorizer']['jwt']['claims']['sub']

    if not sub:
        return create_aws_lambda_response(400, {
            'error_message': 'error_message: token is missing the subject'})
    claims = event['requestContext']['authorizer']['jwt']['claims']
    print("claims")
    print(claims)


    # AWS_SECRET_ACCESS_KEY, AWS_ACCESS_KEY_ID and AWS_SESSION_TOKEN.
    kwargs: Dict[str, str]={}
    role = None

    if 'queryStringParameters' in event and event['queryStringParameters'] is not None:
        if 'role' not in event['queryStringParameters']:
            return create_aws_lambda_response(400, {
                'error_message': 'You must provide a role arn via the query parameter \'role\''})
        role = event['queryStringParameters']['role']

        if 'duration' in event['queryStringParameters']:
            duration = event['queryStringParameters']['duration']
            kwargs['DurationSeconds'] = duration if type(duration) is int else int(duration)

    if 'resource_access' not in claims or role is None or role not in claims['resource_access'] :
        return create_aws_lambda_response(
            403, {'error_message': 'role not allowed'})

    try:
        assumed_account = sts_connection.assume_role(RoleArn=role,RoleSessionName=sub,**kwargs)
        ACCESS_KEY = assumed_account['Credentials']['AccessKeyId']
        SECRET_KEY = assumed_account['Credentials']['SecretAccessKey']
        SESSION_TOKEN = assumed_account['Credentials']['SessionToken']
        EXPIRATION = assumed_account['Credentials']['Expiration']
        EXP_STR= EXPIRATION.isoformat()

        print(f'expiration {EXP_STR}')
        # create service client using the assumed role credentials, e.g. S3
        resource = boto3.resource('s3',
                                  aws_access_key_id=ACCESS_KEY,
                                  aws_secret_access_key=SECRET_KEY,
                                  aws_session_token=SESSION_TOKEN
                                  )
        for bucket in resource.buckets.all():
            print(bucket.name)

        return create_aws_lambda_response(200, [{'account': ACCESS_KEY,
                                                 'password': SECRET_KEY,
                                                 'session_token': SESSION_TOKEN,
                                                 'expiration': EXP_STR
                                                 }])
    except Exception as e:
        return create_aws_lambda_response(500, {'error_message': str(e)})


def create_aws_lambda_response(
    status_code,
    message,
    headers={
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': True,
        'content-type': 'application/json'
    }):
    return {
        'statusCode': status_code,
        'headers': headers,
        'body': json.dumps(message, indent=4, sort_keys=True),
        'message': message
    }


